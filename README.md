# CryptoBot SSH v2

Here, you'll see:

* [Bugs being reported and solved](https://gitlab.com/CryptoBot/v2/issues?label_name%5B%5D=Bug)
* [Improvements to system](https://gitlab.com/CryptoBot/v2/issues?label_name%5B%5D=Improvement)
* [Ideas discussion](https://gitlab.com/CryptoBot/v2/issues?label_name%5B%5D=Idea)
* [Feature proposal](https://gitlab.com/CryptoBot/v2/issues?label_name%5B%5D=Feature+proposal)
* [And much more](https://gitlab.com/CryptoBot/v2/boards)

Feel free to explore and colaborate with comments, ideas, requests and more.

**If you wanna support this project, please, donate any bitcoin amount to 1EYDN3SHhK8iEhNrSiWQNopRHUiXw8ZxbY**